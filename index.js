// SPDX-License-Identifier: CC0-1.0
// Demonstrates a connection to a predefined board

import { SerialPort } from "serialport";
import config from "./config.js";
const boardIdentification = config.boardIdentification;

// listing all ports
const ports = await SerialPort.list();
console.log(ports.map(p => `- ${JSON.stringify(p)}`).join("\n"));

// choosing the right port based on the info available
const filteredPorts = ports.filter((portInfo) => {
	for (const key in boardIdentification) {
		const value = boardIdentification[key];
		if (typeof value.test === "function") { // for regex comparisons
			if (!portInfo[key].match(value)) {
				return false;
			}
		} else {
			if (portInfo[key] !== value) { // for literal string comparisons
				return false;
			}
		}
	}
	return true; // great, we ended up here! so all checks must have gone "OK".
});
const chosenPort = filteredPorts[0];
console.log(`Connecting to ${chosenPort.path} (${chosenPort.friendlyName}, ${chosenPort.serialNumber})`);

// connecting
const port = new SerialPort({
	path: chosenPort.path,
	baudRate: 115200,
});


// sending and receiving here
port.on("data", function (data) {
	console.log("Receive:", data.toString());
});

setInterval(() => {
	const now = (new Date()).toISOString();
	console.log(`Transmit: ${now}`);
	port.write(`${now}`);
}, 2500);
