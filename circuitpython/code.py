# SPDX-License-Identifier: CC0-1.0
# Shows a very basic serial connection that echos all bytes back to the sender.

import usb_cdc

# Set up our serial connection
if usb_cdc.data is None:
    print("USB CDC serial data not found. Make sure you edited boot.py.")
    while True:
        pass
serial = usb_cdc.data

# Looping for echo
while True:
    while serial.in_waiting > 0:
        input = serial.read(serial.in_waiting)
        serial.write(input)
