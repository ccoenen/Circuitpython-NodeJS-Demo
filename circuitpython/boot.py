# SPDX-License-Identifier: CC0-1.0
# All of this can also be found in the circuitpython documentation
# at: https://learn.adafruit.com/customizing-usb-devices-in-circuitpython/circuitpy-midi-serial#usb-serial-console-repl-and-data-3096590-12

import usb_cdc

# for this project, we will want to use a plain serial connection that
# is not used by something else, therefore we will need `data`.
usb_cdc.enable(console=True, data=True)
