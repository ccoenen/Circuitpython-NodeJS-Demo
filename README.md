# Circuitpython NodeJS Demo
Demo-Project for NodeJS serial communication with a circuitpython based chip.

The idea is to connect and communicate with as little manual fiddling as possible.


## Requirements
* NodeJS, written on version 16
* NPM
* some circuitpython based microcontroller board


## Setup
* `npm install`
* copy `config.js.example` to `config.js` and fill in what you need.


## Running
Put the `circuitpython/boot.py` and `circuitpython/code.py` on your microcontroller-board or integrate them into existing projects.


## Tested with
- [Pimoroni Keybow 2040](https://shop.pimoroni.com/products/keybow-2040)


## License
This is a very basic example, that I want everybody to be able to use and benefit from. I license it under CC0 / Public Domain, no attribution required (but if you want, go for it! Just link back to the repository at )


## Stuff to look into later
- Deactivating USB Drive, USB-CDC, USB-MIDI and other unused things. Perhaps even USB-HID? The device should simply just expose a serial port for data. (also needs a way to go back to debug/download mode!)


## References
* https://learn.adafruit.com/customizing-usb-devices-in-circuitpython/circuitpy-midi-serial#usb-serial-console-repl-and-data-3096590-12
* https://learn.adafruit.com/diy-trinkey-no-solder-air-quality-monitor/circuitpython
